<?php

/**
 * Sanitize URL
 * convert string to a more slugish nerdish equivalent
 * 
 * example :
 * input  : J'adore le PHP !
 * output : J-adore-le-php
 *
 * @param string $string The string you wanna convert
 * @return string The converted string
 */
function sanitizeURL($string) {
  $table = array(
    '"' => '-',
    '\'' => '-',
    ' ' => '-'
  );
  $output = strtr($string, $table); // quotes & spaces
  $output = preg_replace('/[^-\w]+/', '', $output); // only alphanum, underscore, dash
  $output = preg_replace('/--+/', '-', $output); // single -
  $output = trim($output, '-');
  return $output;
}

/**
 * Get episodes from a specific podcast URL
 *
 * @param string $url The podcast's XML feed URL
 * @param int $podcastId Unique identifier (optional)
 * @return array An array of all episodes. On error or timeout, returns the $podcastId's value
 */
function getPodcastEpisodes($url, $podcastId=null) {
  $ch = curl_init(); // cause some servers only accept cURL for remote content
  $options = [
    CURLOPT_URL => $url,
    CURLOPT_USERAGENT => "Mozilla/5.0 (X11; Linux x86_64; rv:100.0) Gecko/20100101 Firefox/100.0",
    CURLOPT_HEADER => false,    
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_TIMEOUT => 5,
  ];
  curl_setopt_array($ch, $options);
  $content = curl_exec($ch);
  $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  curl_close($ch);
  if ($content === false OR $http_code !== 200) {
    return $podcastId;
  }
  $episodes = [];
  $xmlObj = new SimpleXMLElement($content, LIBXML_NOCDATA);
  foreach ($xmlObj->channel->item as $episodeObj) {
    $episode = [];
    $episode['title'] = htmlentities($episodeObj->title, ENT_QUOTES | ENT_HTML5);
    $episode['description'] = strip_tags($episodeObj->description);
    $episode['description'] = html_entity_decode($episode['description'], ENT_QUOTES | ENT_XML1, 'UTF-8');
    $episode['description'] = str_replace('&nbsp;', ' ', $episode['description']); // "Les Echos" podcast : WTF fix
    $episode['description'] = trim($episode['description']);
    $episode['descriptionShorter'] = mb_substr($episode['description'], 0, 670, 'UTF-8');
    if($episode['descriptionShorter'] !== $episode['description']) $episode['descriptionShorter'] .= " (...)";
    $episode['descriptionShorter'] = htmlentities($episode['descriptionShorter'], ENT_QUOTES | ENT_HTML5);
    $episode['description'] = htmlentities($episode['description'], ENT_QUOTES | ENT_HTML5);
    $episode['description'] = ucfirst($episode['description']);
    $episode['audioSrc'] = filter_var($episodeObj->enclosure->attributes()->url, FILTER_VALIDATE_URL);
    $episode['pubDate'] = strtotime($episodeObj->pubDate);
    $episode['pubDateStr'] = date('Y-m-d H:i:s', $episode['pubDate']);
    $episode['image'] = filter_var($xmlObj->channel->image->url, FILTER_VALIDATE_URL);
    $episode['podcastTitle'] = htmlentities($xmlObj->channel->title, ENT_QUOTES | ENT_HTML5);
    $episode['link'] = filter_var($episodeObj->link, FILTER_VALIDATE_URL);
    $episode['episodeSlug'] = sanitizeURL(strtolower(iconv('UTF-8', 'ASCII//TRANSLIT', html_entity_decode($episodeObj->title, ENT_QUOTES | ENT_XML1, 'UTF-8'))));
    $episode['podcastId'] = $podcastId;
    $episodes[] = $episode;
  }
  return $episodes;
}