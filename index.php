<?php

require './inc/functions.php';
$app = json_decode(file_get_contents('assets/podcasts.json'));
setlocale(LC_CTYPE, 'en_GB.utf8'); // for iconv's sake

// ---------------------------------------------------------
// ?get-episodes mode
// ---------------------------------------------------------
if (isset($_GET['get-episodes'])) {
  $podcastKey = filter_input(INPUT_GET, 'k', FILTER_VALIDATE_INT);
  if (
    $podcastKey === false OR
    $podcastKey === null OR
    !isset($app->podcasts[$podcastKey])
  ) {
    header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
    exit;
  }
  $content = getPodcastEpisodes($app->podcasts[$podcastKey]->url, $podcastKey);
  header('Content-Type: application/json; charset=utf-8');
  echo json_encode($content);
  exit;
}

?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Podcastor</title>
  <meta name="description" content="Listen to whatever podcast you like, right here, right now.">
  <link rel="stylesheet" href="assets/style.css?<?= filemtime('assets/style.css') ?>">
  <script src="assets/app.js?<?= filemtime('assets/app.js') ?>" defer></script>
</head>
<body>

  <main>
    <details id="new-episodes">
      <summary>New episodes</summary>
      <section>Chargement en cours ...</section>
    </details>
    <?php
      foreach ($app->podcasts as $key => $podcast) :
        $podcast_slug = 'podcast-' . sanitizeURL(strtolower(iconv('UTF-8', 'ASCII//TRANSLIT', $podcast->title)));
    ?>
    <details class="podcast" id="<?= $podcast_slug ?>" data-podcast-key="<?= $key ?>">
      <summary><?= $podcast->title ?></summary>
      <section>Chargement en cours ...</section>
    </details>
    <?php
      endforeach;
    ?>
  </main>

  <footer><a title="Get the source code of this web app" href="https://codeberg.org/jrm-omg/podcastor" target="_blank" rel="noopener noreferrer"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 4.233 4.233"><title>Codeberg logo. Codeberg is a Git hosting platform, and a community of like-minded Free Software and content creators. Ad-free, no third party cookies, no tracking. Hosted in the EU.</title><defs><linearGradient id="a"><stop offset="0" style="stop-color:#2185d0;stop-opacity:0"/><stop offset=".495" style="stop-color:#2185d0;stop-opacity:.48923996"/><stop offset="1" style="stop-color:#2185d0;stop-opacity:.63279623"/></linearGradient><linearGradient xlink:href="#b" id="c" x1="42519.285" x2="42575.336" y1="-7078.789" y2="-6966.931" gradientUnits="userSpaceOnUse"/><linearGradient id="b"><stop offset="0" style="stop-color:#fff;stop-opacity:0"/><stop offset=".495" style="stop-color:#fff;stop-opacity:.30000001"/><stop offset="1" style="stop-color:#fff;stop-opacity:.30000001"/></linearGradient></defs><path d="M42519.285-7078.79a.76.568 0 0 0-.738.675l33.586 125.888a87.182 87.182 0 0 0 39.381-33.763l-71.565-92.52a.76.568 0 0 0-.664-.28z" style="font-variation-settings:normal;opacity:1;fill:url(#c);fill-opacity:1;stroke:none;stroke-width:3.67846;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:2;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke markers fill;stop-color:#000;stop-opacity:1" transform="matrix(.02428 0 0 .02428 -1030.156 172.97)"/><path d="M11249.461-1883.696c-12.74 0-23.067 10.327-23.067 23.067 0 4.333 1.22 8.58 3.522 12.251l19.232-24.863c.138-.18.486-.18.624 0l19.233 24.864a23.068 23.068 0 0 0 3.523-12.252c0-12.74-10.327-23.067-23.067-23.067z" style="opacity:1;fill:#fff;fill-opacity:1;stroke-width:17.0055;paint-order:markers fill stroke;stop-color:#000" transform="translate(-1030.156 172.97) scale(.09176)"/></svg></a></footer>

  <template id="episode-template">
    <article class="episode">
      <h2 tabindex="0"></h2>
      <div class="audio-container">
        <audio src="" controls preload="none"></audio>
      </div>
      <div class="description" tabindex="0"></div>
      <time datetime=""></time>
    </article>
  </template>
  
</body>
</html>