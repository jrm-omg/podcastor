# Podcastor

A simple podcast player, made of PHP & vanilla JS.

Check out the [live demo](https://humanize.me/podcast/) for a working example.

## Concept

- Clean and simple design
- Dependency-free
- Accessible

## Addendum

The app provides a [podcast list example](assets/podcasts.json) that you are free to edit, to fit your needs.

## License

Podcastor is released under the GNU General Public License v3.0 or later, see [here](https://choosealicense.com/licenses/gpl-3.0/) for a description of this license, or see the [LICENSE](LICENSE) file for the full text.

## Credits

Thanks for checking this project out !

If you'd like to see what else I'm up to, consider following me on [Codeberg](https://codeberg.org/jrm-omg) or checking out [my /now personal site](https://humanize.me/now.html).

📻🦫