const app = {

  podcastsList: null,

  /**
   * Started when the DOM's ready :
   * - fetch the podcasts list (podcasts.json)
   * - fetch all episodes for each podcast
   * - add the episodes to the DOM
   */
  init: async function() {
    // fetch podcasts list
    const resp = await fetch('assets/podcasts.json');
    const json = await resp.json();
    app.podcastsList = json.podcasts;
    // fetch 3 podcasts in parallel
    i = 0;
    while (i < app.podcastsList.length) {
      // enqueue
      const queue = [];
      for (let p = 0; p < 3; p++) {
        if (i in app.podcastsList) {
          queue.push(`index.php?get-episodes&k=${i}`);
          i++;
        }
      }
      // fetch'em all
      Promise.all(queue.map(url =>
        fetch(url)
        .then(resp => resp.json())
        .catch(err => console.error(`Dude, I can't fetch this URL : ${url}`, err))
      )).then(podcastsEpisodes => {
        for (const episodes of podcastsEpisodes) {
          if (Array.isArray(episodes)) {
            const key = episodes[0].podcastId;
            app.podcastsList[key].episodes = episodes;
            app.showEpisodes(key);
            app.showLatestEpisodes();
          } else {
            const key = episodes;
            app.showEmptiness(key);
          }
        }
      });
    }
  },

  /**
   * Add podcast's episodes to the DOM
   * @param {number} podcastKey
   */
  showEpisodes: function(podcastKey) {
    let podcastEl = document.querySelector(`[data-podcast-key="${podcastKey}"]`);
    let episodesContainerEl = podcastEl.querySelector('section');
    episodesContainerEl.innerHTML = '';
    // for each episode
    for (const episode of app.podcastsList[podcastKey].episodes) {
      const episodeTplEl = document.querySelector('#episode-template').content.cloneNode(true);
      episodeTplEl.querySelector('h2').innerHTML = episode.title;
      episodeTplEl.querySelector('audio').src = episode.audioSrc;
      episodeTplEl.querySelector('.description').innerHTML = `<p>${episode.descriptionShorter} (<a href="${episode.link}" target="_blank" rel="noopener noreferrer">source</a>)</p>`;
      episodeTplEl.querySelector('time').setAttribute('datetime', episode.pubDateStr);
      episodeTplEl.querySelector('time').innerHTML = `${episode.pubDateStr}`;
      episodesContainerEl.append(episodeTplEl);
    }
    podcastEl.classList.add('loaded');
  },

  /**
   * Add a quick sorry message to the DOM
   * @param {number} podcastKey 
   */
  showEmptiness: function(podcastKey) {
    let podcastEl = document.querySelector(`[data-podcast-key="${podcastKey}"]`);
    let episodesContainerEl = podcastEl.querySelector('section');
    episodesContainerEl.innerHTML = 'Mhh, looks like this podcast is not working, for now 🤷';
    podcastEl.classList.add('error-when-loaded');
  },

  /**
   * Add the latest 15 episodes, from all podcasts, to the DOM
   */
  showLatestEpisodes: function() {
    let allEpisodes = [];
    for (const key in app.podcastsList) {
      if ('episodes' in app.podcastsList[key]) {
        for (const episode of app.podcastsList[key].episodes) {
          allEpisodes.push(episode);
        }
      }
    }
    allEpisodes.sort((a, b) => (a.pubDate > b.pubDate) ? -1 : 1);
    let last15Episodes = allEpisodes.slice(0, 15);
    let episodesContainerEl = document.querySelector('#new-episodes section');
    episodesContainerEl.innerHTML = '';
    // for each episode
    for (const episode of last15Episodes) {
      const episodeTplEl = document.querySelector('#episode-template').content.cloneNode(true);
      episodeTplEl.querySelector('h2').innerHTML = `${episode.title} (${episode.podcastTitle})`;
      episodeTplEl.querySelector('audio').src = episode.audioSrc;
      episodeTplEl.querySelector('.description').innerHTML = `<p>${episode.descriptionShorter} (<a href="${episode.link}" target="_blank" rel="noopener noreferrer">source</a>)</p>`;
      episodeTplEl.querySelector('time').setAttribute('datetime', episode.pubDateStr);
      episodeTplEl.querySelector('time').innerHTML = `${episode.pubDateStr}`;
      episodesContainerEl.append(episodeTplEl);
    }
    document.querySelector('#new-episodes').classList.add('loaded');
  },

}

// ready to serve, zog zog
document.addEventListener('DOMContentLoaded', app.init);